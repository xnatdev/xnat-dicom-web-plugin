package org.nrg.xnat.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.jackson.module.JsonDicomWebDeserializationModule;
import org.nrg.xnat.model.DicomContent;
import org.nrg.xnat.model.DicomObject;
import org.nrg.xnat.model.DicomPersonNames;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.dcm4che3.data.VR.*;

public class TestDeserialization {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void registerModule() {
        objectMapper.registerModule(JsonDicomWebDeserializationModule.build());
    }

    @Test
    public void testBasicDeserialization() throws IOException {
        final DicomObject root = new DicomObject();
        final Map<String, DicomContent> tags = root.getTagMap();

        tags.put("00080005", DicomContent.ofSingleton("ISO_IR192").vr(CS));
        tags.put("00080020", DicomContent.ofSingleton("20130409").vr(DT));
        tags.put("00080030", DicomContent.ofSingleton("131600.0000").vr(TM));
        tags.put("00080050", DicomContent.ofSingleton("11235813").vr(SH));
        tags.put("00080061", DicomContent.ofStrings("CT", "PET").vr(CS));
        tags.put("00080090", DicomContent.ofSingleton(new DicomPersonNames().alphabetic("^Bob^^Dr.")).vr(PN));
        tags.put("00090010", DicomContent.ofSingleton("Vendor A").vr(LO));
        tags.put("00091002", DicomContent.ofSingleton("z0x9c8v7").vr(UN));
        tags.put("00100010", DicomContent.ofSingleton(new DicomPersonNames().alphabetic("Wang^XiaoDong").ideographic("王^小東")).vr(PN));

        final DicomObject sequenceItem0 = new DicomObject();
        final DicomObject sequenceItem1 = new DicomObject();
        sequenceItem0.getTagMap().put("00100020", DicomContent.ofSingleton("54321").vr(LO));
        sequenceItem0.getTagMap().put("00100021", DicomContent.ofSingleton("Hospital B").vr(LO));
        sequenceItem1.getTagMap().put("00100020", DicomContent.ofSingleton("24680").vr(LO));
        sequenceItem1.getTagMap().put("00100021", DicomContent.ofSingleton("Hospital C").vr(LO));
        tags.put("00101002", DicomContent.ofSequenceItems(sequenceItem0, sequenceItem1).vr(SQ));
        tags.put("00201206", DicomContent.ofSingleton(4).vr(IS));

        assertEquals(root, objectMapper.readValue(FileUtils.getFile("src", "test", "resources", "test.json"), DicomObject.class));
    }

}
