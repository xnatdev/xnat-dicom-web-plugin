package org.nrg.xnat.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dcm4che3.data.VR;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.jackson.module.JsonDicomWebSerializationModule;
import org.nrg.xnat.model.DicomContent;
import org.nrg.xnat.model.DicomObject;
import org.nrg.xnat.model.DicomPersonNames;

import static org.junit.Assert.assertEquals;

public class TestSerialization {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeClass
    public static void registerModule() {
        objectMapper.registerModule(JsonDicomWebSerializationModule.build());
    }

    @Test
    public void testBasicSerialization() throws JsonProcessingException {
        final DicomObject root = new DicomObject();
        final DicomObject sequenceItem = new DicomObject();
        sequenceItem.getTagMap().put("00100020", DicomContent.ofSingleton("54321").vr(VR.LO));
        root.getTagMap().put("00200011", DicomContent.ofSingleton(1).vr(VR.IS));
        root.getTagMap().put("00100010", DicomContent.ofSingleton(new DicomPersonNames().alphabetic("TEST")).vr(VR.PN));
        root.getTagMap().put("00101002", DicomContent.ofSingleton(sequenceItem).vr(VR.SQ));
        assertEquals(
                "{\"00200011\":{\"vr\":\"IS\",\"Value\":[1]},\"00100010\":{\"vr\":\"PN\",\"Value\":[{\"Alphabetic\":\"TEST\"}]},\"00101002\":{\"vr\":\"SQ\",\"Value\":[{\"00100020\":{\"vr\":\"LO\",\"Value\":[\"54321\"]}}]}}",
                objectMapper.writeValueAsString(root)
        );
    }

    @Test // Tests part18:F.2.5 (empty values as null and empty sequence items as empty JSON objects)
    public void testModelNullValues() throws JsonProcessingException {
        final DicomObject root = new DicomObject();
        root.getTagMap().put("00181020",DicomContent.ofStrings("v1.0", null, "v2.0").vr(VR.LO));
        root.getTagMap().put("00101002", DicomContent.ofSingleton(new DicomObject()).vr(VR.SQ));
        assertEquals("{\"00181020\":{\"vr\":\"LO\",\"Value\":[\"v1.0\",null,\"v2.0\"]},\"00101002\":{\"vr\":\"SQ\",\"Value\":[{}]}}", objectMapper.writeValueAsString(root));
    }

}
