package org.nrg.xnat.plugin;

import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "dicom-web-plugin", name = "XNAT 1.7 DICOM Web Plugin", description = "This is the XNAT 1.7 DICOM Web Plugin.")
public class DicomWebPlugin {}
