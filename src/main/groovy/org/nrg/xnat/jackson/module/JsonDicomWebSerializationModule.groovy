package org.nrg.xnat.jackson.module

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.xnat.jackson.serializer.JsonDicomObjectSerializer
import org.nrg.xnat.model.DicomObject

class JsonDicomWebSerializationModule {

    static SimpleModule build() {
        final SimpleModule module = new SimpleModule("JSON_XNAT_DicomWeb_Serializers")

        module.addSerializer(DicomObject, new JsonDicomObjectSerializer(DicomObject))

        module
    }
}
