package org.nrg.xnat.jackson.module

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.xnat.jackson.deserializer.JsonDicomContentDeserializer
import org.nrg.xnat.jackson.deserializer.JsonDicomObjectDeserializer
import org.nrg.xnat.model.DicomContent
import org.nrg.xnat.model.DicomObject

class JsonDicomWebDeserializationModule {

    static SimpleModule build() {
        final SimpleModule module = new SimpleModule("JSON_XNAT_DicomWeb_Deserializers")

        module.addDeserializer(DicomObject, new JsonDicomObjectDeserializer())
        module.addDeserializer(DicomContent, new JsonDicomContentDeserializer())

        module
    }
}
