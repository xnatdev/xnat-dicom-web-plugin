package org.nrg.xnat.jackson.deserializer

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.dcm4che3.data.VR
import org.nrg.xnat.model.DicomContent

class JsonDicomContentDeserializer extends CustomDeserializer<DicomContent> {

    @Override
    DicomContent deserialize(ObjectCodec objectCodec, JsonNode node) throws IOException, JsonProcessingException {
        final VR vr = readEnumByName(node.get('vr').asText(), VR)
        final DicomContent<?> content = DicomContent.ofVR(vr)

        content.setVr(vr)
        setStringIfNonnull(node, 'BulkDataURI', content.&setBulkDataUri)
        setStringIfNonnull(node, 'InlineBinary', content.&setInlineBinary)
        if (fieldNonnull(node, 'Value')) {
            setObjectList(node, 'Value', objectCodec, content.type, content.&setValue)
        }

        content
    }


}
