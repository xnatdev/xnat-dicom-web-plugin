package org.nrg.xnat.jackson.deserializer

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.model.DicomContent
import org.nrg.xnat.model.DicomObject

class JsonDicomObjectDeserializer extends CustomDeserializer<DicomObject> {

    @Override
    DicomObject deserialize(ObjectCodec objectCodec, JsonNode node) throws IOException, JsonProcessingException {
        final DicomObject dicomObject = new DicomObject()

        dicomObject.tagMap = node.fields().collectEntries { entry ->
            [(entry.key) : readObject(entry.value, objectCodec, DicomContent)]
        }

        dicomObject
    }

}
