package org.nrg.xnat.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

@SuppressWarnings("GrMethodMayBeStatic")
abstract class CustomDeserializer<T> extends StdDeserializer<T> {

    protected CustomDeserializer(Class<T> aClass) {
        super(aClass)
    }

    protected CustomDeserializer() {
        this(null)
    }

    @Override
    T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return deserialize(p.codec, p.codec.readTree(p) as JsonNode)
    }

    abstract T deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException

    protected void setStringIfNonnull(JsonNode parentNode, String field, Closure method) {
        if (fieldNonnull(parentNode, field)) method(parentNode.get(field).asText())
    }

    protected void setObjectList(JsonNode parentNode, String field, ObjectCodec codec, Class objectClass, Closure method) {
        if (fieldNonnull(parentNode, field)) {
            method((codec as ObjectMapper).readerFor(objectClass).readValues(parentNode.get(field).toString()).readAll())
        }
    }

    protected boolean fieldNonnull(JsonNode parentNode, String field) {
        parentNode.has(field) && !parentNode.get(field).isNull()
    }

    protected <X> X readObject(JsonNode node, ObjectCodec codec, Class<X> objectClass) {
        (codec as ObjectMapper).readValue(node.toString(), objectClass)
    }

    protected <X extends Enum> X readEnumByName(String representation, Class<X> enumClass) {
        final X value = EnumSet.allOf(enumClass).find { it.name() == representation }
        if (value == null) {
            throw new RuntimeException("Enum not found of class ${enumClass} with name() == ${representation}")
        }
        value
    }

}
