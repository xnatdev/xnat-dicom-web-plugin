package org.nrg.xnat.jackson.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.nrg.xnat.model.DicomObject

class JsonDicomObjectSerializer extends StdSerializer<DicomObject> {

    JsonDicomObjectSerializer(Class<DicomObject> t) {
        super(t)
    }

    @Override
    void serialize(DicomObject value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeStartObject()
        value.tagMap.each { key, val ->
            gen.writeObjectField(key, val)
        }
        gen.writeEndObject()
    }

}
