package org.nrg.xnat

class DicomValidationException extends Exception {

    DicomValidationException(String message) {
        super(message)
    }

}
