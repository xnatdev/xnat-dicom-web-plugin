package org.nrg.xnat.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import org.dcm4che3.data.VR
import org.nrg.xnat.DicomValidationException

import static org.dcm4che3.data.VR.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class DicomContent<T> {

    public static final List<VR> STRING_VRS      = [AE, AS, AT, CS, DA, DT, LO, LT, OB, OD, OF, OW, SH, ST, TM, UI, UN, UT]
    public static final List<VR> DOUBLE_VRS      = [DS, FL, FD]
    public static final List<VR> LONG_VRS        = [IS, SL, SS, UL, US]
    public static final List<VR> PERSON_NAME_VRS = [PN]
    public static final List<VR> SEQUENCE_VRS    = [SQ]
    public static final Map<Class, List<VR>> VR_CLASS_MAP = [(String) : STRING_VRS, (Double) : DOUBLE_VRS, (Long) : LONG_VRS, (DicomPersonNames) : PERSON_NAME_VRS, (DicomObject) : SEQUENCE_VRS]

    @JsonIgnore Class<T> type
    VR vr
    @JsonProperty('BulkDataURI') String bulkDataUri
    @JsonProperty('InlineBinary') String inlineBinary
    @JsonProperty('Value') List<T> value

    DicomContent(Class<T> type) {
        this.type = type
    }

    static DicomContent<?> ofVR(VR vr) {
        new DicomContent<?>(VR_CLASS_MAP.find{ vr in it.value }.key)
    }

    static DicomContent<?> ofSingleton(Object value) {
        new DicomContent<?>(value.class).value(Collections.singletonList(value))
    }

    static DicomContent<String> ofStrings(String... strings) {
        new DicomContent<>(String).value(Arrays.asList(strings))
    }

    static DicomContent<String> ofDoubles(Double... doubles) {
        new DicomContent<>(Double).value(Arrays.asList(doubles))
    }

    static DicomContent<String> ofLongs(Long... longs) {
        new DicomContent<>(Long).value(Arrays.asList(longs))
    }

    static DicomContent<String> ofPersonNames(DicomPersonNames... names) {
        new DicomContent<>(DicomPersonNames).value(Arrays.asList(names))
    }

    static DicomContent<String> ofSequenceItems(DicomObject... objects) {
        new DicomContent<>(DicomObject).value(Arrays.asList(objects))
    }

    DicomContent<T> vr(VR vr) {
        setVr(vr)
        this
    }

    DicomContent<T> bulkDataUri(String uri) {
        setBulkDataUri(uri)
        this
    }

    DicomContent<T> inlineBinary(String binary) {
        setInlineBinary(binary)
        this
    }

    DicomContent<T> value(List<T> value) {
        setValue(value)
        this
    }

    void validate() throws DicomValidationException {
        validatePossibleDataChildren()
        validateTypeVRMatch()
    }

    void validatePossibleDataChildren() throws DicomValidationException { // I wasn't sure what to call this. It validates that at most one of BulkDataURI, InlineBinary, and Value are is present.
        int numValuesPresent = 0
        if (bulkDataUri != null) numValuesPresent++
        if (inlineBinary != null) numValuesPresent++
        if (value != null && !value.isEmpty()) numValuesPresent++
        if (numValuesPresent > 1) throw new DicomValidationException('At most one of BulkDataURI, InlineBinary, and Value can be set.')
    }

    void validateTypeVRMatch() throws DicomValidationException {
        final List<VR> acceptableVRsForType = VR_CLASS_MAP[type] ?: []
        if (!(vr in acceptableVRsForType)) throw new DicomValidationException("DicomContent cannot store a list of values using the type ${type.simpleName} with corresponding VR ${vr}.")
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof DicomContent)) return false

        DicomContent that = (DicomContent) o

        if (bulkDataUri != that.bulkDataUri) return false
        if (inlineBinary != that.inlineBinary) return false
        if (value != that.value) return false
        if (vr != that.vr) return false

        return true
    }

    int hashCode() {
        int result
        result = (vr != null ? vr.hashCode() : 0)
        result = 31 * result + (bulkDataUri != null ? bulkDataUri.hashCode() : 0)
        result = 31 * result + (inlineBinary != null ? inlineBinary.hashCode() : 0)
        result = 31 * result + (value != null ? value.hashCode() : 0)
        return result
    }

}
