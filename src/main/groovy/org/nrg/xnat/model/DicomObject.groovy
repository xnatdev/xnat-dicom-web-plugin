package org.nrg.xnat.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DicomObject {

    Map<String, DicomContent> tagMap = [:]

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof DicomObject)) return false

        DicomObject that = (DicomObject) o

        if (tagMap != that.tagMap) return false

        return true
    }

    int hashCode() {
        return (tagMap != null ? tagMap.hashCode() : 0)
    }

}
